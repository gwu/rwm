#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <xcb/xcb.h>
#include <xcb/xcb_atom.h>
#include <xcb/xcb_ewmh.h>

#include "misc.h"
#include "vector.h"
#include "window.h"

void window_focus
	( xcb_connection_t *c
	, xcb_window_t fw
	, xcb_window_t w
	) {
	debugf(1, "focused new window: %u", w);

	xcb_set_input_focus
		( c
		, XCB_INPUT_FOCUS_POINTER_ROOT
		, w
		, XCB_CURRENT_TIME
		);

	fw = w;
}

void window_manage
	( xcb_connection_t *c
	, xcb_window_t w
	) {
	debugf(1, "manage new window: %u", w);

    xcb_grab_button(c, 0, w, XCB_EVENT_MASK_BUTTON_PRESS,
        XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC, XCB_NONE, XCB_NONE,
		XCB_BUTTON_INDEX_ANY, XCB_MOD_MASK_ANY);

    uint32_t mask[] =
		{ XCB_EVENT_MASK_EXPOSURE				// XCB_CW_EVENT_MASK
		| XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY	// XCB_DESTRUCT_NOTIFY
        | XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT	// XCB_MAP_REQUEST
		| XCB_EVENT_MASK_BUTTON_PRESS			// XCB_BUTTON_PRESS
		| XCB_EVENT_MASK_BUTTON_RELEASE			// XCB_BUTTON_RELEASE
		};

	xcb_change_window_attributes(c, w, XCB_CW_EVENT_MASK, mask);
}
