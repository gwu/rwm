#ifndef HANDLES_H
#define HANDLES_H

#include "vector.h"

extern int (*event_handlers[])
	( xcb_connection_t *
	, xcb_screen_t *
	, vector_t *
	, xcb_window_t
	, xcb_generic_event_t *
	);

#endif // HANDLES_H
