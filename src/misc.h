#ifndef MISC_H
#define MISC_H

#define debugf(v, fmt, ...) \
	do { \
		if (v <= verbosity) \
			fprintf(stderr, "%s:%d:%s(): " fmt "\n" \
				, __FILE__, __LINE__, __func__ ,__VA_ARGS__); \
	} while (0)


#define debugs(v, s) \
	do { \
		if (v <= verbosity) \
			fprintf(stderr, "%s:%d:%s(): " s "\n", __FILE__, __LINE__, __func__); \
	} while (0)

int verbosity;

#endif // MISC_H
