/* Vector implementation by
 * https://github.com/Sweets
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "vector.h"

vector_t *vector_new() {
	vector_t *vector = (vector_t *)malloc(sizeof(vector_t));
	vector->capacity = 1;
	vector->size = 0;
	vector->elements = malloc(sizeof(void *));
	return vector;
}

void *vector_at(vector_t *vector, unsigned int i) {
	if (i >= vector->size) return NULL;
	return vector->elements[i];
}

void *vector_pop(vector_t *vector) {
	if (vector->size == 0) return NULL;
	void *tmp = vector->elements[vector->size-1];
	vector->elements[vector->size--] = NULL;
	return tmp;
}

void vector_push(vector_t *vector, void *data) {
	if (vector->size == vector->capacity) {
		vector->capacity *= 2;
		vector->elements =
			realloc(vector->elements, sizeof(void *) * vector->capacity);
	}

	vector->elements[vector->size++] = (void *)data;
}

void vector_erase(vector_t *vector, unsigned int i) {
	if (i >= vector->size) return;

	for (; i < vector->size; i++)
		vector->elements[i] = vector->elements[i + 1];

	vector->elements[vector->size--] = NULL;

	if ((vector->size * 2) == vector->capacity) {
		vector->capacity /= 2;
		vector->elements =
			realloc(vector->elements, sizeof(void *) * vector->capacity);
	}
}

void vector_switch(vector_t *vector, unsigned int a, unsigned int b) {
	if (a >= vector->size || b >= vector->size) return;
	void *tmp = vector->elements[a];
	vector->elements[a] = vector->elements[b];
	vector->elements[b] = tmp;
}

void vector_free(vector_t *vector) {
	free(vector->elements);
	free(vector);
}
