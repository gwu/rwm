#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <xcb/xcb.h>
#include <xcb/xcb_atom.h>
#include <xcb/xcb_ewmh.h>

#include "misc.h"

#include "mouse.h"
#include "window.h"

int handle_button_press
	( xcb_connection_t * c
	, xcb_screen_t * s
	, vector_t *winv
	, xcb_window_t fw
	, xcb_generic_event_t * _e
	) {
	xcb_button_press_event_t *e = (xcb_button_press_event_t *)_e;
	debugf(1, "button press event on: %u", e->event);
	if (!e->event || e->event == s->root) return 0;
	window_focus(c, fw, e->event);
	return 0;
}

#if 0

void handle_motion_notify
	( xcb_connection_t * c
	, xcb_screen_t * s
	, xcb_generic_event_t * e
	) {
}

void handle_button_release
	( xcb_connection_t * c
	, xcb_screen_t * s
	, xcb_generic_event_t * e
	) {
	/* focus(win); */
	xcb_ungrab_pointer(c, XCB_CURRENT_TIME);
}

#endif
