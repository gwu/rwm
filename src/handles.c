#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <xcb/xcb.h>
#include <xcb/xcb_atom.h>
#include <xcb/xcb_ewmh.h>

#include "misc.h"
#include "vector.h"
#include "handles.h"
#include "mouse.h"
#include "window.h"

int handle_map_request
	( xcb_connection_t *c
	, xcb_screen_t *screen
	, vector_t *winv
	, xcb_window_t fw
	, xcb_generic_event_t *_e
	) {
    xcb_map_request_event_t *e = (xcb_map_request_event_t *)_e;
	/* if (e->override_redirect) return; */
    xcb_window_t w = e->window;

    xcb_map_window(c, w);
	debugs(1, "new window mapped");
	window_manage(c, w);
	vector_push(winv, &w);

	if (w != fw && winv->size == 1)
		window_focus(c, fw, w);

	return 0;
}

int handle_destroy_notify
	( xcb_connection_t *c
	, xcb_screen_t *s
	, vector_t *winv
	, xcb_window_t fw
	, xcb_generic_event_t *_e
	) {
    xcb_destroy_notify_event_t *e = (xcb_destroy_notify_event_t *)_e;
	xcb_kill_client(c, e->window);

	debugs(1, "client killed");

	xcb_window_t _w;
	size_t i = 0;
    for (; i < winv->size; i++) {
        _w = *(xcb_window_t *)vector_at(winv, i);
        if (_w == e->window) {
            vector_erase(winv, i);
			if (e->window == fw && winv->size > 0) {
				window_focus
					( c
					, fw
					, *(xcb_window_t *)vector_at(winv, winv->size-1)
					);
			} else {
				window_focus(c, fw, XCB_WINDOW_NONE);
			}
        }
	}

	return 0;
}

int (*event_handlers[])
	( xcb_connection_t *
	, xcb_screen_t *
	, vector_t *
	, xcb_window_t
	, xcb_generic_event_t *
	) =
	{ [XCB_MAP_REQUEST]		= handle_map_request
	, [XCB_DESTROY_NOTIFY]	= handle_destroy_notify
	, [XCB_BUTTON_PRESS]	= handle_button_press
	/* , [XCB_MOTION_NOTIFY]	= handle_motion_notify */
	/* , [XCB_BUTTON_RELEASE]	= handle_button_release */
	};
