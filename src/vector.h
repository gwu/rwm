#ifndef VECTOR_H
#define VECTOR_H

/* Vector implementation by
 * https://github.com/Sweets
 *
 * slight edits
 * - gwu
 */

typedef struct {
    void **elements;
    unsigned int capacity;
    unsigned int size;
} vector_t;

vector_t *vector_new(void);
void vector_free(vector_t *);

void *vector_at(vector_t *, unsigned int);
void *vector_pop(vector_t *);

void vector_push(vector_t *, void *);
void vector_erase(vector_t *, unsigned int);
void vector_switch(vector_t *, unsigned int, unsigned int);

#endif // VECTOR_H
