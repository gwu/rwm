#ifndef ARG_H
#define ARG_H

#include <stdlib.h>

/**
 * Copy me if you can.
 * by 20h
 *
 * Give me a challenge.
 * - gwu
**/

#define popts() \
	char *argv0; \
	for ( argv0 = *argv, argv++, argc-- \
		; argv[0] && argv[0][0] == '-' && argv[0][1] \
		; argc--, argv++ \
		) { \
		char **argv_; \
		int brk_; \
	 \
		if (argv[0][1] == '-' && argv[0][2] == '\0') { \
			argv++; \
			argc--; \
			break; \
		} \
	 \
		for ( brk_ = 0, argv[0]++, argv_ = argv \
			; argv[0][0] && !brk_\
			; argv[0]++ \
			) { \
			if (argv_ != argv) break; \
			switch (argv[0][0])
#define popts_end \
		} \
	}

#define eoptarg(f) ( \
	(argv[0][1] == '\0' && argv[1] == NULL) \
		? ((f), abort(), (char *)0) \
		: (brk_ = 1, (argv[0][1] != '\0') \
			? (&argv[0][1]) \
			: (argc--, argv++, argv[0]) \
		) \
)

#define optarg() ( \
	(argv[0][1] == '\0' && argv[1] == NULL) \
	? (char *)0 \
	: (brk_ = 1 , (argv[0][1] != '\0') \
		? (&argv[0][1]) \
		: (argc--, argv++, argv[0]) \
	) \
)

#endif // ARG_H
