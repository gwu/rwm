#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <xcb/xcb.h>
#include <xcb/xcb_atom.h>
#include <xcb/xcb_ewmh.h>

#include "arg.h"
#include "vector.h"
#include "handles.h"
#include "misc.h"

int main(int argc, char **argv) {
	int ret = 0;

	popts () {
		case 'v': verbosity++; break;
		case 'h': printf("%s: -{v,h}\n", argv0); return 0;
	} popts_end

    xcb_connection_t *c = xcb_connect(NULL, NULL);
    if (xcb_connection_has_error(c)) {
		return 1;
	};

    xcb_screen_t *s =
		xcb_setup_roots_iterator(xcb_get_setup(c)).data;

    uint32_t mask[] =
		{ XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY	// XCB_DESTRUCT_NOTIFY
        | XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT	// XCB_MAP_REQUEST
		};

	xcb_void_cookie_t cookie =
		xcb_change_window_attributes_checked(c, s->root, XCB_CW_EVENT_MASK, mask);

    if (xcb_request_check(c, cookie)) {
		fputs("another wm seems to be running\n", stderr);
		return 1;
	}

	vector_t *winv = vector_new();
	xcb_window_t fw = XCB_WINDOW_NONE;

	// event loop
	while (!ret) {
		xcb_generic_event_t *e = xcb_wait_for_event(c);
		if (e) {
			unsigned int et = e->response_type & ~0x80;

			debugf(2, "new event: %u", et);

			if (event_handlers[et])
				ret = event_handlers[et](c, s, winv, fw, e);
		}
		xcb_flush(c);
		free(e);
	}

	return ret;
}
