#ifndef WINDOW_H
#define WINDOW_H

void window_focus(xcb_connection_t *, xcb_window_t, xcb_window_t);
void window_manage(xcb_connection_t *, xcb_window_t);

#endif // WINDOW_H
