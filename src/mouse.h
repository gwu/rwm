#ifndef MOUSE_H
#define MOUSE_H

#include "vector.h"

int handle_button_press
	( xcb_connection_t *
	, xcb_screen_t *
	, vector_t *
	, xcb_window_t
	, xcb_generic_event_t *
	);

#if 0

void handle_motion_notify
	( xcb_connection_t *
	, xcb_screen_t *
	, xcb_generic_event_t *
	);

void handle_button_release
	( xcb_connection_t *
	, xcb_screen_t *
	, xcb_generic_event_t *
	);

#endif

#endif // MOUSE_H
