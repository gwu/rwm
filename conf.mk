CC = 	clang
LD = 	$(CC)

SRCPREFIX ?= src
BUILDPREFIX ?= bin
DOCPREFIX ?= doc

PREFIX = /usr
MANPREFIX = $(PREFIX)/share/man
BINPREFIX = $(PREFIX)/bin

VERSION = $(shell git describe --always --dirty)

# OPTFLAGS = -O3
CFLAGS		=	$(OPTFLAGS) \
	-I /usr/include\
	-Wall -Wextra \
	-pedantic \
	-std=c17 \
	-DVERSION=\"$(VERSION)\" \
	-DDEBUG
	# -Werror \
	# -pedantic-errors \

LDLIBS = $(shell pkg-config --libs xcb-util)
LDFLAGS = -s $(LDLIBS)
